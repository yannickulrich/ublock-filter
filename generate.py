#!/usr/bin/env python
import subprocess
import glob
import os


devFilters = [
    "github_copycats.txt",
    "stackoverflow_copycats.txt",
    "npm_copycats.txt",
    "wikipedia_copycats.txt"
]


def fetch_filterlist(url):
    proc = subprocess.Popen(
        ['wget', '-O', '-', url], stdout=subprocess.PIPE
    )
    stdout = proc.communicate()[0]
    return stdout.decode().split('\n')


def mklist():
    devBase = (
        "https://raw.githubusercontent.com"
        "/quenhus/uBlock-Origin-dev-filter/main/data/"
    )

    lst = set()
    for i in devFilters:
        lst.update(fetch_filterlist(devBase + i))

    for i in glob.glob('lists/*.txt'):
        with open(i) as fp:
            lst.update(fp.read().split('\n'))

    return lst


def to_css_attr(url):
    return url.replace("*://", "").replace("*.", ".").replace("/*", "")


def to_duckduckgo(url):
    url_splitted = url.replace("*://", "").split("/", 1)
    if len(url_splitted) > 1 and url_splitted[1] != "*":
        # Has a non-wildcard path, aka path != "/*"
        return f'duckduckgo.com###links>.result[data-domain$="{to_css_attr(url_splitted[0])}"]:has(a[href*="{to_css_attr(url)}"])'
    return f'duckduckgo.com###links>.result[data-domain$="{to_css_attr(url)}"]'


header = """! Title: modified uBlock-Origin-dev-filter
! Expires: 1 day
! Description: Filters to block copycat-websites from search engines.
"""


def main():
    try:
        os.mkdir('public')
    except FileExistsError:
        pass

    with open('public/filters.txt', 'w') as fp:
        fp.write(header)
        for pat in mklist():
            fp.write(pat + '\n')
            if not pat.startswith('!'):
                fp.write(to_duckduckgo(pat) + '\n')


if __name__ == "__main__":
    main()
